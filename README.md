# Docker Runner

A Docker image suitable for use as a CI/CD runner.  The idea is to have a catch-all runner image useable for all builds.